var express = require("express");
var bodyParser = require("body-parser");
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var rutas = require("./rutas.js")(app);

var server = app.listen(49160, function () {
    console.log("Escuchando en el puerto %s...", server.address().port);
});
