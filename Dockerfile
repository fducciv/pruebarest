FROM node:12

WORKDIR .

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 49160

CMD [ "node", "./rutas/app.js"]
